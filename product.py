import sys
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QApplication, QPushButton, QTextEdit
from PyQt5.QtWidgets import QLabel, QLCDNumber, QLineEdit
import PyQt5.QtGui as QtGui
from PyQt5.QtWidgets import QInputDialog, QColorDialog

choise = ['Москва']

Moskva_Marshala_Katukova = {'Ботинки': '8599',
                            'Куртка': '12999',
                            'Шапка': '299',
                            'Перчатки': '199',
                            'Джинсы': '7299',
                            }
Moskva_Porechnaya = {'Футболка': '1699',
                     'Джемпер': '1299',
                     'Шорты': '999',
                     'Брюки': '1999',
                     'Кроссовки': '6999', }
Moskva_Altufevskoe = {'Майка': '599',
                      'Куртка': '12999',
                      'Рубашка': '1499',
                      'Жилет': '2499',
                      'Полуботинки': '4999', }
Moskva_Varshavskoe = {'Шорты': '999',
                      'Кеды': '1599',
                      'Шапка': '299',
                      'Перчатки': '199',
                      'Джинсы': '7299', }
Moskva_Dmitrovskoe = {'Сапоги': '5699',
                      'Жилет': '2499',
                      'Кроссовки': '6999',
                      'Перчатки': '199',
                      'Джинсы': '7299', }

Peterburg_Petergofskoe = {'Ботинки': '8599',
                          'Джемпер': '1299',
                          'Рубашка': '1499',
                          'Джинсы': '7299',
                          'Перчатки': '199'}
Peterburg_Grazhdanskij = {'Футболка': '1699',
                          'Куртка': '12999',
                          'Кроссовки': '6999',
                          'Брюки': '1999',
                          'Джинсы': '7299'}
Peterburg_Pulkovskoe = {'Майка': '599',
                        'Кеды': '1599',
                        'Кроссовки': '6999',
                        'Перчатки': '199',
                        'Брюки': '1999'}
Peterburg_Kultury = {'Шорты': '999',
                     'Жилет': '2499',
                     'Шапка': '299',
                     'Рубашка': '1499',
                     'Полуботинки': '4999'}
Peterburg_Savushkina = {'Сапоги': '5699',
                        'Куртка': '12999',
                        'Шорты': '999',
                        'Жилет': '2499',
                        'Джинсы': '7299'}


class Example(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.setGeometry(300, 300, 1000, 500)
        self.setWindowTitle('Наличие товара')

        self.button_1 = QPushButton(self)
        self.button_1.move(20, 160)
        self.button_1.setText("ул. Маршала Катукова, д.18")
        self.button_1.clicked.connect(self.run)

        self.button_2 = QPushButton(self)
        self.button_2.move(20, 200)
        self.button_2.setText("ул. Маршала Катукова, д.18")
        self.button_2.clicked.connect(self.run)

        self.button_3 = QPushButton(self)
        self.button_3.move(20, 240)
        self.button_3.setText("ул. Маршала Катукова, д.18")
        self.button_3.clicked.connect(self.run)

        self.button_4 = QPushButton(self)
        self.button_4.move(20, 280)
        self.button_4.setText("ул. Маршала Катукова, д.18")
        self.button_4.clicked.connect(self.run)

        self.button_5 = QPushButton(self)
        self.button_5.move(20, 320)
        self.button_5.setText("ул. Маршала Катукова, д.18")
        self.button_5.clicked.connect(self.run)

        self.button_search = QPushButton(self)
        self.button_search.move(900, 40)
        self.button_search.setText("Поиск")
        self.button_search.clicked.connect(self.dialog)

        self.label_city = QLabel(self)
        self.label_city.setText("ул. Маршала Катукова, д.18")
        self.label_city.move(20, 10)

        self.label_name_1 = QLabel(self)
        self.label_name_1.setText("Выберите магазин")
        self.label_name_1.move(200, 160)

        self.label_name_2 = QLabel(self)
        self.label_name_2.setText("Выберите магазин")
        self.label_name_2.move(200, 200)

        self.label_name_3 = QLabel(self)
        self.label_name_3.setText("Выберите магазин")
        self.label_name_3.move(200, 240)

        self.label_name_4 = QLabel(self)
        self.label_name_4.setText("Выберите магазин")
        self.label_name_4.move(200, 280)

        self.label_name_5 = QLabel(self)
        self.label_name_5.setText("Выберите магазин")
        self.label_name_5.move(200, 320)

        self.label_price_1 = QLabel(self)
        self.label_price_1.setText("Выберите магазин")
        self.label_price_1.move(400, 160)

        self.label_price_2 = QLabel(self)
        self.label_price_2.setText("Выберите магазин")
        self.label_price_2.move(400, 200)

        self.label_price_3 = QLabel(self)
        self.label_price_3.setText("Выберите магазин")
        self.label_price_3.move(400, 240)

        self.label_price_4 = QLabel(self)
        self.label_price_4.setText("Выберите магазин")
        self.label_price_4.move(400, 280)

        self.label_price_5 = QLabel(self)
        self.label_price_5.setText("Выберите магазин")
        self.label_price_5.move(400, 320)

        self.label_search_shop_1 = QLabel(self)
        self.label_search_shop_1.setText("                                                                            ")
        self.label_search_shop_1.move(800, 80)

        self.label_search_shop_2 = QLabel(self)
        self.label_search_shop_2.setText("                                                                            ")
        self.label_search_shop_2.move(800, 120)

        self.label_search_shop_3 = QLabel(self)
        self.label_search_shop_3.setText("                                                                            ")
        self.label_search_shop_3.move(800, 160)

        self.label_search_shop_4 = QLabel(self)
        self.label_search_shop_4.setText("                                                                            ")
        self.label_search_shop_4.move(800, 200)

        self.label_search_shop_5 = QLabel(self)
        self.label_search_shop_5.setText("                                                                            ")
        self.label_search_shop_5.move(800, 240)

        self.label_name_shop = QLabel(self)
        self.label_name_shop.setText("                                                                            ")
        self.label_name_shop.move(30, 90)

        self.label_name = QLabel(self)
        self.label_name.setText("Название")
        self.label_name.move(200, 120)

        self.button_name = QPushButton(self)
        self.button_name.move(195, 80)
        self.button_name.setText("Сортировать")
        self.button_name.clicked.connect(self.sort_name)

        self.label_price = QLabel(self)
        self.label_price.setText("Цена")
        self.label_price.move(400, 120)

        self.button_price = QPushButton(self)
        self.button_price.move(395, 80)
        self.button_price.setText("Сортировать")
        self.button_price.clicked.connect(self.sort_price)

        self.button_city = QPushButton(self)
        self.button_city.resize(1000, 500)
        self.button_city.move(0, 0)
        self.button_city.setText("Выберите ваш город")
        self.button_city.clicked.connect(self.you_city)

        self.show()

    def dialog(self):
        global choise
        i, okBtnPressed = QInputDialog.getText(
            self, "Введите артикул", "Артикул товара"
        )
        if okBtnPressed:
            self.label_search_shop_1.setText("                                                                        ")

            self.label_search_shop_2.setText("                                                                        ")

            self.label_search_shop_3.setText("                                                                        ")

            self.label_search_shop_4.setText("                                                                        ")

            self.label_search_shop_5.setText("                                                                        ")
            total = 1
            if choise[0] == 'Москва':
                global Moskva_Marshala_Katukova, Moskva_Altufevskoe, Moskva_Dmitrovskoe, Moskva_Porechnaya, \
                    Moskva_Varshavskoe
                if i in Moskva_Marshala_Katukova:
                    if total == 1:
                        self.label_search_shop_1.setText('ул. Маршала Катукова, д.18')
                        total += 1
                    elif total == 2:
                        self.label_search_shop_2.setText('ул. Маршала Катукова, д.18')
                        total += 1
                    elif total == 3:
                        self.label_search_shop_3.setText('ул. Маршала Катукова, д.18')
                        total += 1
                    elif total == 4:
                        self.label_search_shop_4.setText('ул. Маршала Катукова, д.18')
                        total += 1
                    elif total == 5:
                        self.label_search_shop_5.setText('ул. Маршала Катукова, д.18')
                        total += 1
                if i in Moskva_Altufevskoe:
                    if total == 1:
                        self.label_search_shop_1.setText('Алтуфьевское ш., д.79')
                        total += 1
                    elif total == 2:
                        self.label_search_shop_2.setText('Алтуфьевское ш., д.79')
                        total += 1
                    elif total == 3:
                        self.label_search_shop_3.setText('Алтуфьевское ш., д.79')
                        total += 1
                    elif total == 4:
                        self.label_search_shop_4.setText('Алтуфьевское ш., д.79')
                        total += 1
                    elif total == 5:
                        self.label_search_shop_5.setText('Алтуфьевское ш., д.79')
                        total += 1
                if i in Moskva_Dmitrovskoe:
                    if total == 1:
                        self.label_search_shop_1.setText('Дмитровское ш., 37')
                        total += 1
                    elif total == 2:
                        self.label_search_shop_2.setText('Дмитровское ш., 37')
                        total += 1
                    elif total == 3:
                        self.label_search_shop_3.setText('Дмитровское ш., 37')
                        total += 1
                    elif total == 4:
                        self.label_search_shop_4.setText('Дмитровское ш., 37')
                        total += 1
                    elif total == 5:
                        self.label_search_shop_5.setText('Дмитровское ш., 37')
                        total += 1
                if i in Moskva_Porechnaya:
                    if total == 1:
                        self.label_search_shop_1.setText('ул. Поречная, д.10')
                        total += 1
                    elif total == 2:
                        self.label_search_shop_2.setText('ул. Поречная, д.10')
                        total += 1
                    elif total == 3:
                        self.label_search_shop_3.setText('ул. Поречная, д.10')
                        total += 1
                    elif total == 4:
                        self.label_search_shop_4.setText('ул. Поречная, д.10')
                        total += 1
                    elif total == 5:
                        self.label_search_shop_5.setText('ул. Поречная, д.10')
                        total += 1
                if i in Moskva_Varshavskoe:
                    if total == 1:
                        self.label_search_shop_1.setText('Варшавское ш., д.129А')
                        total += 1
                    elif total == 2:
                        self.label_search_shop_2.setText('Варшавское ш., д.129А')
                        total += 1
                    elif total == 3:
                        self.label_search_shop_3.setText('Варшавское ш., д.129А')
                        total += 1
                    elif total == 4:
                        self.label_search_shop_4.setText('Варшавское ш., д.129А')
                        total += 1
                    elif total == 5:
                        self.label_search_shop_5.setText('Варшавское ш., д.129А')
                        total += 1
            if choise[0] == 'Санкт-Петербург':
                global Peterburg_Grazhdanskij, Peterburg_Kultury, Peterburg_Petergofskoe, Peterburg_Pulkovskoe, \
                    Peterburg_Savushkina
                if i in Peterburg_Grazhdanskij:
                    if total == 1:
                        self.label_search_shop_1.setText('Гражданский пр-т, д. 31')
                        total += 1
                    elif total == 2:
                        self.label_search_shop_2.setText('Гражданский пр-т, д. 31')
                        total += 1
                    elif total == 3:
                        self.label_search_shop_3.setText('Гражданский пр-т, д. 31')
                        total += 1
                    elif total == 4:
                        self.label_search_shop_4.setText('Гражданский пр-т, д. 31')
                        total += 1
                    elif total == 5:
                        self.label_search_shop_5.setText('Гражданский пр-т, д. 31')
                        total += 1
                if i in Peterburg_Kultury:
                    if total == 1:
                        self.label_search_shop_1.setText('пр-т Культуры, д. 1А')
                        total += 1
                    elif total == 2:
                        self.label_search_shop_2.setText('пр-т Культуры, д. 1А')
                        total += 1
                    elif total == 3:
                        self.label_search_shop_3.setText('пр-т Культуры, д. 1А')
                        total += 1
                    elif total == 4:
                        self.label_search_shop_4.setText('пр-т Культуры, д. 1А')
                        total += 1
                    elif total == 5:
                        self.label_search_shop_5.setText('пр-т Культуры, д. 1А')
                        total += 1
                if i in Peterburg_Savushkina:
                    if total == 1:
                        self.label_search_shop_1.setText('ул. Савушкина, д. 141')
                        total += 1
                    elif total == 2:
                        self.label_search_shop_2.setText('ул. Савушкина, д. 141')
                        total += 1
                    elif total == 3:
                        self.label_search_shop_3.setText('ул. Савушкина, д. 141')
                        total += 1
                    elif total == 4:
                        self.label_search_shop_4.setText('ул. Савушкина, д. 141')
                        total += 1
                    elif total == 5:
                        self.label_search_shop_5.setText('ул. Савушкина, д. 141')
                        total += 1
                if i in Peterburg_Petergofskoe:
                    if total == 1:
                        self.label_search_shop_1.setText('Петергофское шоссе д. 51')
                        total += 1
                    elif total == 2:
                        self.label_search_shop_2.setText('Петергофское шоссе д. 51')
                        total += 1
                    elif total == 3:
                        self.label_search_shop_3.setText('Петергофское шоссе д. 51')
                        total += 1
                    elif total == 4:
                        self.label_search_shop_4.setText('Петергофское шоссе д. 51')
                        total += 1
                    elif total == 5:
                        self.label_search_shop_5.setText('Петергофское шоссе д. 51')
                        total += 1
                if i in Peterburg_Pulkovskoe:
                    if total == 1:
                        self.label_search_shop_1.setText('Пулковское шоссе д. 19Б')
                        total += 1
                    elif total == 2:
                        self.label_search_shop_2.setText('Пулковское шоссе д. 19Б')
                        total += 1
                    elif total == 3:
                        self.label_search_shop_3.setText('Пулковское шоссе д. 19Б')
                        total += 1
                    elif total == 4:
                        self.label_search_shop_4.setText('Пулковское шоссе д. 19Б')
                        total += 1
                    elif total == 5:
                        self.label_search_shop_5.setText('Пулковское шоссе д. 19Б')
                        total += 1

    def you_city(self):
        global choice
        i, okBtnPressed = QInputDialog.getItem(
            self,
            "Выберите ваш город",
            "Откуда ты?",
            ("Москва", "Санкт-Петербург"),
            1,
            False
        )
        if okBtnPressed:
            self.label_city.setText(i)
            self.button_city.resize(150, 25)
            self.button_city.move(20, 30)
            if i == 'Москва':
                self.button_1.setText("ул. Маршала Катукова, д.18")

                self.button_2.setText("ул. Поречная, д.10")

                self.button_3.setText("Алтуфьевское ш., д.79")

                self.button_4.setText("Варшавское ш., д.129А")

                self.button_5.setText("Дмитровское ш., 37")

            elif i == 'Санкт-Петербург':
                self.button_1.setText("Петергофское шоссе д. 51")

                self.button_2.setText("Гражданский пр-т, д. 31")

                self.button_3.setText("Пулковское шоссе д. 19Б")

                self.button_4.setText("пр-т Культуры, д. 1А")

                self.button_5.setText("ул. Савушкина, д. 141")
        del choise[:]
        choise.append(i)

    def run(self):
        global choise
        self.label_name_shop.setText(self.sender().text())
        if choise[0] == 'Москва':
            global Moskva_Marshala_Katukova, Moskva_Altufevskoe, Moskva_Dmitrovskoe, Moskva_Porechnaya, \
                Moskva_Varshavskoe
            if self.sender().text() == 'ул. Маршала Катукова, д.18':
                total = 1
                for name, price in Moskva_Marshala_Katukova.items():
                    if total == 1:
                        self.label_name_1.setText(name)
                        self.label_price_1.setText(price)
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(name)
                        self.label_price_2.setText(price)
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(name)
                        self.label_price_3.setText(price)
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(name)
                        self.label_price_4.setText(price)
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(name)
                        self.label_price_5.setText(price)
                        total += 1
            elif self.sender().text() == 'ул. Поречная, д.10':
                total = 1
                for name, price in Moskva_Porechnaya.items():
                    if total == 1:
                        self.label_name_1.setText(name)
                        self.label_price_1.setText(price)
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(name)
                        self.label_price_2.setText(price)
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(name)
                        self.label_price_3.setText(price)
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(name)
                        self.label_price_4.setText(price)
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(name)
                        self.label_price_5.setText(price)
                        total += 1
            elif self.sender().text() == 'Алтуфьевское ш., д.79':
                total = 1
                for name, price in Moskva_Altufevskoe.items():
                    if total == 1:
                        self.label_name_1.setText(name)
                        self.label_price_1.setText(price)
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(name)
                        self.label_price_2.setText(price)
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(name)
                        self.label_price_3.setText(price)
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(name)
                        self.label_price_4.setText(price)
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(name)
                        self.label_price_5.setText(price)
                        total += 1
            elif self.sender().text() == 'Варшавское ш., д.129А':
                total = 1
                for name, price in Moskva_Varshavskoe.items():
                    if total == 1:
                        self.label_name_1.setText(name)
                        self.label_price_1.setText(price)
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(name)
                        self.label_price_2.setText(price)
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(name)
                        self.label_price_3.setText(price)
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(name)
                        self.label_price_4.setText(price)
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(name)
                        self.label_price_5.setText(price)
                        total += 1
            elif self.sender().text() == 'Дмитровское ш., 37':
                total = 1
                for name, price in Moskva_Dmitrovskoe.items():
                    if total == 1:
                        self.label_name_1.setText(name)
                        self.label_price_1.setText(price)
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(name)
                        self.label_price_2.setText(price)
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(name)
                        self.label_price_3.setText(price)
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(name)
                        self.label_price_4.setText(price)
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(name)
                        self.label_price_5.setText(price)
                        total += 1
        if choise[0] == 'Санкт-Петербург':
            global Peterburg_Grazhdanskij, Peterburg_Kultury, Peterburg_Petergofskoe, Peterburg_Pulkovskoe, \
                    Peterburg_Savushkina
            if self.sender().text() == 'Петергофское шоссе д. 51':
                total = 1
                for name, price in Peterburg_Petergofskoe.items():
                    if total == 1:
                        self.label_name_1.setText(name)
                        self.label_price_1.setText(price)
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(name)
                        self.label_price_2.setText(price)
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(name)
                        self.label_price_3.setText(price)
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(name)
                        self.label_price_4.setText(price)
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(name)
                        self.label_price_5.setText(price)
                        total += 1
            elif self.sender().text() == 'Гражданский пр-т, д. 31':
                total = 1
                for name, price in Peterburg_Grazhdanskij.items():
                    if total == 1:
                        self.label_name_1.setText(name)
                        self.label_price_1.setText(price)
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(name)
                        self.label_price_2.setText(price)
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(name)
                        self.label_price_3.setText(price)
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(name)
                        self.label_price_4.setText(price)
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(name)
                        self.label_price_5.setText(price)
                        total += 1
            elif self.sender().text() == 'Пулковское шоссе д. 19Б':
                total = 1
                for name, price in Peterburg_Pulkovskoe.items():
                    if total == 1:
                        self.label_name_1.setText(name)
                        self.label_price_1.setText(price)
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(name)
                        self.label_price_2.setText(price)
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(name)
                        self.label_price_3.setText(price)
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(name)
                        self.label_price_4.setText(price)
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(name)
                        self.label_price_5.setText(price)
                        total += 1
            elif self.sender().text() == 'пр-т Культуры, д. 1А':
                total = 1
                for name, price in Peterburg_Kultury.items():
                    if total == 1:
                        self.label_name_1.setText(name)
                        self.label_price_1.setText(price)
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(name)
                        self.label_price_2.setText(price)
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(name)
                        self.label_price_3.setText(price)
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(name)
                        self.label_price_4.setText(price)
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(name)
                        self.label_price_5.setText(price)
                        total += 1
            elif self.sender().text() == 'ул. Савушкина, д. 141':
                total = 1
                for name, price in Peterburg_Savushkina.items():
                    if total == 1:
                        self.label_name_1.setText(name)
                        self.label_price_1.setText(price)
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(name)
                        self.label_price_2.setText(price)
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(name)
                        self.label_price_3.setText(price)
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(name)
                        self.label_price_4.setText(price)
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(name)
                        self.label_price_5.setText(price)
                        total += 1

    def sort_name(self):
        global choise
        if choise[0] == 'Москва':
            global Moskva_Marshala_Katukova, Moskva_Altufevskoe, Moskva_Dmitrovskoe, Moskva_Porechnaya, \
                Moskva_Varshavskoe
            if self.label_name_shop.text() == 'ул. Маршала Катукова, д.18':
                new_list = sorted(Moskva_Marshala_Katukova)
                total = 1
                for name in new_list:
                    if total == 1:
                        self.label_name_1.setText(name)
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(name)
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(name)
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(name)
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(name)
                        total += 1
                total = 1
                for name in new_list:
                    if total == 1:
                        self.label_price_1.setText(Moskva_Marshala_Katukova[name])
                        total += 1
                    elif total == 2:
                        self.label_price_2.setText(Moskva_Marshala_Katukova[name])
                        total += 1
                    elif total == 3:
                        self.label_price_3.setText(Moskva_Marshala_Katukova[name])
                        total += 1
                    elif total == 4:
                        self.label_price_4.setText(Moskva_Marshala_Katukova[name])
                        total += 1
                    elif total == 5:
                        self.label_price_5.setText(Moskva_Marshala_Katukova[name])
                        total += 1
            elif self.label_name_shop.text() == 'ул. Поречная, д.10':
                new_list = sorted(Moskva_Porechnaya)
                total = 1
                for name in new_list:
                    if total == 1:
                        self.label_name_1.setText(name)
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(name)
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(name)
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(name)
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(name)
                        total += 1
                total = 1
                for name in new_list:
                    if total == 1:
                        self.label_price_1.setText(Moskva_Porechnaya[name])
                        total += 1
                    elif total == 2:
                        self.label_price_2.setText(Moskva_Porechnaya[name])
                        total += 1
                    elif total == 3:
                        self.label_price_3.setText(Moskva_Porechnaya[name])
                        total += 1
                    elif total == 4:
                        self.label_price_4.setText(Moskva_Porechnaya[name])
                        total += 1
                    elif total == 5:
                        self.label_price_5.setText(Moskva_Porechnaya[name])
                        total += 1
            elif self.label_name_shop.text() == 'Алтуфьевское ш., д.79':
                new_list = sorted(Moskva_Altufevskoe)
                total = 1
                for name in new_list:
                    if total == 1:
                        self.label_name_1.setText(name)
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(name)
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(name)
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(name)
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(name)
                        total += 1
                total = 1
                for name in new_list:
                    if total == 1:
                        self.label_price_1.setText(Moskva_Altufevskoe[name])
                        total += 1
                    elif total == 2:
                        self.label_price_2.setText(Moskva_Altufevskoe[name])
                        total += 1
                    elif total == 3:
                        self.label_price_3.setText(Moskva_Altufevskoe[name])
                        total += 1
                    elif total == 4:
                        self.label_price_4.setText(Moskva_Altufevskoe[name])
                        total += 1
                    elif total == 5:
                        self.label_price_5.setText(Moskva_Altufevskoe[name])
                        total += 1
            elif self.label_name_shop.text() == 'Варшавское ш., д.129А':
                new_list = sorted(Moskva_Varshavskoe)
                total = 1
                for name in new_list:
                    if total == 1:
                        self.label_name_1.setText(name)
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(name)
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(name)
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(name)
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(name)
                        total += 1
                total = 1
                for name in new_list:
                    if total == 1:
                        self.label_price_1.setText(Moskva_Varshavskoe[name])
                        total += 1
                    elif total == 2:
                        self.label_price_2.setText(Moskva_Varshavskoe[name])
                        total += 1
                    elif total == 3:
                        self.label_price_3.setText(Moskva_Varshavskoe[name])
                        total += 1
                    elif total == 4:
                        self.label_price_4.setText(Moskva_Varshavskoe[name])
                        total += 1
                    elif total == 5:
                        self.label_price_5.setText(Moskva_Varshavskoe[name])
                        total += 1
            elif self.label_name_shop.text() == 'Дмитровское ш., 37':
                new_list = sorted(Moskva_Dmitrovskoe)
                total = 1
                for name in new_list:
                    if total == 1:
                        self.label_name_1.setText(name)
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(name)
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(name)
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(name)
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(name)
                        total += 1
                total = 1
                for name in new_list:
                    if total == 1:
                        self.label_price_1.setText(Moskva_Dmitrovskoe[name])
                        total += 1
                    elif total == 2:
                        self.label_price_2.setText(Moskva_Dmitrovskoe[name])
                        total += 1
                    elif total == 3:
                        self.label_price_3.setText(Moskva_Dmitrovskoe[name])
                        total += 1
                    elif total == 4:
                        self.label_price_4.setText(Moskva_Dmitrovskoe[name])
                        total += 1
                    elif total == 5:
                        self.label_price_5.setText(Moskva_Dmitrovskoe[name])
                        total += 1
        if choise[0] == 'Санкт-Петербург':
            global Peterburg_Grazhdanskij, Peterburg_Kultury, Peterburg_Petergofskoe, Peterburg_Pulkovskoe, \
                    Peterburg_Savushkina
            if self.label_name_shop.text() == 'Петергофское шоссе д. 51':
                new_list = sorted(Peterburg_Petergofskoe)
                total = 1
                for name in new_list:
                    if total == 1:
                        self.label_name_1.setText(name)
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(name)
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(name)
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(name)
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(name)
                        total += 1
                total = 1
                for name in new_list:
                    if total == 1:
                        self.label_price_1.setText(Peterburg_Petergofskoe[name])
                        total += 1
                    elif total == 2:
                        self.label_price_2.setText(Peterburg_Petergofskoe[name])
                        total += 1
                    elif total == 3:
                        self.label_price_3.setText(Peterburg_Petergofskoe[name])
                        total += 1
                    elif total == 4:
                        self.label_price_4.setText(Peterburg_Petergofskoe[name])
                        total += 1
                    elif total == 5:
                        self.label_price_5.setText(Peterburg_Petergofskoe[name])
                        total += 1
            elif self.label_name_shop.text() == 'Гражданский пр-т, д. 31':
                new_list = sorted(Peterburg_Grazhdanskij)
                total = 1
                for name in new_list:
                    if total == 1:
                        self.label_name_1.setText(name)
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(name)
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(name)
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(name)
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(name)
                        total += 1
                total = 1
                for name in new_list:
                    if total == 1:
                        self.label_price_1.setText(Peterburg_Grazhdanskij[name])
                        total += 1
                    elif total == 2:
                        self.label_price_2.setText(Peterburg_Grazhdanskij[name])
                        total += 1
                    elif total == 3:
                        self.label_price_3.setText(Peterburg_Grazhdanskij[name])
                        total += 1
                    elif total == 4:
                        self.label_price_4.setText(Peterburg_Grazhdanskij[name])
                        total += 1
                    elif total == 5:
                        self.label_price_5.setText(Peterburg_Grazhdanskij[name])
                        total += 1
            elif self.label_name_shop.text() == 'Пулковское шоссе д. 19Б':
                new_list = sorted(Peterburg_Pulkovskoe)
                total = 1
                for name in new_list:
                    if total == 1:
                        self.label_name_1.setText(name)
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(name)
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(name)
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(name)
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(name)
                        total += 1
                total = 1
                for name in new_list:
                    if total == 1:
                        self.label_price_1.setText(Peterburg_Pulkovskoe[name])
                        total += 1
                    elif total == 2:
                        self.label_price_2.setText(Peterburg_Pulkovskoe[name])
                        total += 1
                    elif total == 3:
                        self.label_price_3.setText(Peterburg_Pulkovskoe[name])
                        total += 1
                    elif total == 4:
                        self.label_price_4.setText(Peterburg_Pulkovskoe[name])
                        total += 1
                    elif total == 5:
                        self.label_price_5.setText(Peterburg_Pulkovskoe[name])
                        total += 1
            elif self.label_name_shop.text() == 'пр-т Культуры, д. 1А':
                new_list = sorted(Peterburg_Kultury)
                total = 1
                for name in new_list:
                    if total == 1:
                        self.label_name_1.setText(name)
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(name)
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(name)
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(name)
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(name)
                        total += 1
                total = 1
                for name in new_list:
                    if total == 1:
                        self.label_price_1.setText(Peterburg_Kultury[name])
                        total += 1
                    elif total == 2:
                        self.label_price_2.setText(Peterburg_Kultury[name])
                        total += 1
                    elif total == 3:
                        self.label_price_3.setText(Peterburg_Kultury[name])
                        total += 1
                    elif total == 4:
                        self.label_price_4.setText(Peterburg_Kultury[name])
                        total += 1
                    elif total == 5:
                        self.label_price_5.setText(Peterburg_Kultury[name])
                        total += 1
            elif self.label_name_shop.text() == 'ул. Савушкина, д. 141':
                new_list = sorted(Peterburg_Savushkina)
                total = 1
                for name in new_list:
                    if total == 1:
                        self.label_name_1.setText(name)
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(name)
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(name)
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(name)
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(name)
                        total += 1
                total = 1
                for name in new_list:
                    if total == 1:
                        self.label_price_1.setText(Peterburg_Savushkina[name])
                        total += 1
                    elif total == 2:
                        self.label_price_2.setText(Peterburg_Savushkina[name])
                        total += 1
                    elif total == 3:
                        self.label_price_3.setText(Peterburg_Savushkina[name])
                        total += 1
                    elif total == 4:
                        self.label_price_4.setText(Peterburg_Savushkina[name])
                        total += 1
                    elif total == 5:
                        self.label_price_5.setText(Peterburg_Savushkina[name])
                        total += 1

    def sort_price(self):
        global choise
        if choise[0] == 'Москва':
            global Moskva_Marshala_Katukova, Moskva_Altufevskoe, Moskva_Dmitrovskoe, Moskva_Porechnaya, \
                Moskva_Varshavskoe
            if self.label_name_shop.text() == 'ул. Маршала Катукова, д.18':
                new_list = {}
                ultra_new_list = []
                for name, price in Moskva_Marshala_Katukova.items():
                    new_list[price] = name
                for price in new_list:
                    ultra_new_list.append(int(price))
                ultra_new_first = sorted(ultra_new_list)
                total = 1
                for name in ultra_new_first:
                    if total == 1:
                        self.label_price_1.setText(str(name))
                        total += 1
                    elif total == 2:
                        self.label_price_2.setText(str(name))
                        total += 1
                    elif total == 3:
                        self.label_price_3.setText(str(name))
                        total += 1
                    elif total == 4:
                        self.label_price_4.setText(str(name))
                        total += 1
                    elif total == 5:
                        self.label_price_5.setText(str(name))
                        total += 1
                total = 1
                for name in ultra_new_first:
                    if total == 1:
                        self.label_name_1.setText(new_list[str(name)])
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(new_list[str(name)])
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(new_list[str(name)])
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(new_list[str(name)])
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(new_list[str(name)])
                        total += 1
            elif self.label_name_shop.text() == 'ул. Поречная, д.10':
                new_list = {}
                ultra_new_list = []
                for name, price in Moskva_Porechnaya.items():
                    new_list[price] = name
                for price in new_list:
                    ultra_new_list.append(int(price))
                ultra_new_first = sorted(ultra_new_list)
                total = 1
                for name in ultra_new_first:
                    if total == 1:
                        self.label_price_1.setText(str(name))
                        total += 1
                    elif total == 2:
                        self.label_price_2.setText(str(name))
                        total += 1
                    elif total == 3:
                        self.label_price_3.setText(str(name))
                        total += 1
                    elif total == 4:
                        self.label_price_4.setText(str(name))
                        total += 1
                    elif total == 5:
                        self.label_price_5.setText(str(name))
                        total += 1
                total = 1
                for name in ultra_new_first:
                    if total == 1:
                        self.label_name_1.setText(new_list[str(name)])
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(new_list[str(name)])
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(new_list[str(name)])
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(new_list[str(name)])
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(new_list[str(name)])
                        total += 1
            elif self.label_name_shop.text() == 'Алтуфьевское ш., д.79':
                new_list = {}
                ultra_new_list = []
                for name, price in Moskva_Altufevskoe.items():
                    new_list[price] = name
                for price in new_list:
                    ultra_new_list.append(int(price))
                ultra_new_first = sorted(ultra_new_list)
                total = 1
                for name in ultra_new_first:
                    if total == 1:
                        self.label_price_1.setText(str(name))
                        total += 1
                    elif total == 2:
                        self.label_price_2.setText(str(name))
                        total += 1
                    elif total == 3:
                        self.label_price_3.setText(str(name))
                        total += 1
                    elif total == 4:
                        self.label_price_4.setText(str(name))
                        total += 1
                    elif total == 5:
                        self.label_price_5.setText(str(name))
                        total += 1
                total = 1
                for name in ultra_new_first:
                    if total == 1:
                        self.label_name_1.setText(new_list[str(name)])
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(new_list[str(name)])
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(new_list[str(name)])
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(new_list[str(name)])
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(new_list[str(name)])
                        total += 1
            elif self.label_name_shop.text() == 'Варшавское ш., д.129А':
                new_list = {}
                ultra_new_list = []
                for name, price in Moskva_Varshavskoe.items():
                    new_list[price] = name
                for price in new_list:
                    ultra_new_list.append(int(price))
                ultra_new_first = sorted(ultra_new_list)
                total = 1
                for name in ultra_new_first:
                    if total == 1:
                        self.label_price_1.setText(str(name))
                        total += 1
                    elif total == 2:
                        self.label_price_2.setText(str(name))
                        total += 1
                    elif total == 3:
                        self.label_price_3.setText(str(name))
                        total += 1
                    elif total == 4:
                        self.label_price_4.setText(str(name))
                        total += 1
                    elif total == 5:
                        self.label_price_5.setText(str(name))
                        total += 1
                total = 1
                for name in ultra_new_first:
                    if total == 1:
                        self.label_name_1.setText(new_list[str(name)])
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(new_list[str(name)])
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(new_list[str(name)])
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(new_list[str(name)])
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(new_list[str(name)])
                        total += 1
            elif self.label_name_shop.text() == 'Дмитровское ш., 37':
                new_list = {}
                ultra_new_list = []
                for name, price in Moskva_Dmitrovskoe.items():
                    new_list[price] = name
                for price in new_list:
                    ultra_new_list.append(int(price))
                ultra_new_first = sorted(ultra_new_list)
                total = 1
                for name in ultra_new_first:
                    if total == 1:
                        self.label_price_1.setText(str(name))
                        total += 1
                    elif total == 2:
                        self.label_price_2.setText(str(name))
                        total += 1
                    elif total == 3:
                        self.label_price_3.setText(str(name))
                        total += 1
                    elif total == 4:
                        self.label_price_4.setText(str(name))
                        total += 1
                    elif total == 5:
                        self.label_price_5.setText(str(name))
                        total += 1
                total = 1
                for name in ultra_new_first:
                    if total == 1:
                        self.label_name_1.setText(new_list[str(name)])
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(new_list[str(name)])
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(new_list[str(name)])
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(new_list[str(name)])
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(new_list[str(name)])
                        total += 1
        if choise[0] == 'Санкт-Петербург':
            global Peterburg_Grazhdanskij, Peterburg_Kultury, Peterburg_Petergofskoe, Peterburg_Pulkovskoe, \
                    Peterburg_Savushkina
            if self.label_name_shop.text() == 'Петергофское шоссе д. 51':
                new_list = {}
                ultra_new_list = []
                for name, price in Peterburg_Petergofskoe.items():
                    new_list[price] = name
                for price in new_list:
                    ultra_new_list.append(int(price))
                ultra_new_first = sorted(ultra_new_list)
                total = 1
                for name in ultra_new_first:
                    if total == 1:
                        self.label_price_1.setText(str(name))
                        total += 1
                    elif total == 2:
                        self.label_price_2.setText(str(name))
                        total += 1
                    elif total == 3:
                        self.label_price_3.setText(str(name))
                        total += 1
                    elif total == 4:
                        self.label_price_4.setText(str(name))
                        total += 1
                    elif total == 5:
                        self.label_price_5.setText(str(name))
                        total += 1
                total = 1
                for name in ultra_new_first:
                    if total == 1:
                        self.label_name_1.setText(new_list[str(name)])
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(new_list[str(name)])
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(new_list[str(name)])
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(new_list[str(name)])
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(new_list[str(name)])
                        total += 1
            elif self.label_name_shop.text() == 'Гражданский пр-т, д. 31':
                new_list = {}
                ultra_new_list = []
                for name, price in Peterburg_Grazhdanskij.items():
                    new_list[price] = name
                for price in new_list:
                    ultra_new_list.append(int(price))
                ultra_new_first = sorted(ultra_new_list)
                total = 1
                for name in ultra_new_first:
                    if total == 1:
                        self.label_price_1.setText(str(name))
                        total += 1
                    elif total == 2:
                        self.label_price_2.setText(str(name))
                        total += 1
                    elif total == 3:
                        self.label_price_3.setText(str(name))
                        total += 1
                    elif total == 4:
                        self.label_price_4.setText(str(name))
                        total += 1
                    elif total == 5:
                        self.label_price_5.setText(str(name))
                        total += 1
                total = 1
                for name in ultra_new_first:
                    if total == 1:
                        self.label_name_1.setText(new_list[str(name)])
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(new_list[str(name)])
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(new_list[str(name)])
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(new_list[str(name)])
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(new_list[str(name)])
                        total += 1
            elif self.label_name_shop.text() == 'Пулковское шоссе д. 19Б':
                new_list = {}
                ultra_new_list = []
                for name, price in Peterburg_Pulkovskoe.items():
                    new_list[price] = name
                for price in new_list:
                    ultra_new_list.append(int(price))
                ultra_new_first = sorted(ultra_new_list)
                total = 1
                for name in ultra_new_first:
                    if total == 1:
                        self.label_price_1.setText(str(name))
                        total += 1
                    elif total == 2:
                        self.label_price_2.setText(str(name))
                        total += 1
                    elif total == 3:
                        self.label_price_3.setText(str(name))
                        total += 1
                    elif total == 4:
                        self.label_price_4.setText(str(name))
                        total += 1
                    elif total == 5:
                        self.label_price_5.setText(str(name))
                        total += 1
                total = 1
                for name in ultra_new_first:
                    if total == 1:
                        self.label_name_1.setText(new_list[str(name)])
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(new_list[str(name)])
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(new_list[str(name)])
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(new_list[str(name)])
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(new_list[str(name)])
                        total += 1
            elif self.label_name_shop.text() == 'пр-т Культуры, д. 1А':
                new_list = {}
                ultra_new_list = []
                for name, price in Peterburg_Kultury.items():
                    new_list[price] = name
                for price in new_list:
                    ultra_new_list.append(int(price))
                ultra_new_first = sorted(ultra_new_list)
                total = 1
                for name in ultra_new_first:
                    if total == 1:
                        self.label_price_1.setText(str(name))
                        total += 1
                    elif total == 2:
                        self.label_price_2.setText(str(name))
                        total += 1
                    elif total == 3:
                        self.label_price_3.setText(str(name))
                        total += 1
                    elif total == 4:
                        self.label_price_4.setText(str(name))
                        total += 1
                    elif total == 5:
                        self.label_price_5.setText(str(name))
                        total += 1
                total = 1
                for name in ultra_new_first:
                    if total == 1:
                        self.label_name_1.setText(new_list[str(name)])
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(new_list[str(name)])
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(new_list[str(name)])
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(new_list[str(name)])
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(new_list[str(name)])
                        total += 1
            elif self.label_name_shop.text() == 'ул. Савушкина, д. 141':
                new_list = {}
                ultra_new_list = []
                for name, price in Peterburg_Savushkina.items():
                    new_list[price] = name
                for price in new_list:
                    ultra_new_list.append(int(price))
                ultra_new_first = sorted(ultra_new_list)
                total = 1
                for name in ultra_new_first:
                    if total == 1:
                        self.label_price_1.setText(str(name))
                        total += 1
                    elif total == 2:
                        self.label_price_2.setText(str(name))
                        total += 1
                    elif total == 3:
                        self.label_price_3.setText(str(name))
                        total += 1
                    elif total == 4:
                        self.label_price_4.setText(str(name))
                        total += 1
                    elif total == 5:
                        self.label_price_5.setText(str(name))
                        total += 1
                total = 1
                for name in ultra_new_first:
                    if total == 1:
                        self.label_name_1.setText(new_list[str(name)])
                        total += 1
                    elif total == 2:
                        self.label_name_2.setText(new_list[str(name)])
                        total += 1
                    elif total == 3:
                        self.label_name_3.setText(new_list[str(name)])
                        total += 1
                    elif total == 4:
                        self.label_name_4.setText(new_list[str(name)])
                        total += 1
                    elif total == 5:
                        self.label_name_5.setText(new_list[str(name)])
                        total += 1


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    ex.show()
    sys.exit(app.exec())
